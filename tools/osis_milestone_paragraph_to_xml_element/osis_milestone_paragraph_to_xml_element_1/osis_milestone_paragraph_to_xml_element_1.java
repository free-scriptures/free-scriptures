/* Copyright (C) 2014-2022  Stephan Kreutzer
 *
 * This file is part of osis_milestone_paragraph_to_xml_element_1.
 *
 * osis_milestone_paragraph_to_xml_element_1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * osis_milestone_paragraph_to_xml_element_1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with osis_milestone_paragraph_to_xml_element_1. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/osis_milestone_paragraph_to_xml_element_1.java
 * @todo This could be made into a generic tool.
 * @todo No implicit <paragraph/> at start of chapter, if the
 *     first verse is without <milestone/> marker for paragraph.
 * @author Stephan Kreutzer
 * @since 2022-05-02
 */



import java.io.File;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.FileOutputStream;
import javax.xml.stream.XMLInputFactory;
import java.io.InputStream;
import java.io.FileInputStream;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.events.XMLEvent;
import javax.xml.stream.events.StartDocument;
import javax.xml.namespace.QName;
import javax.xml.stream.events.Attribute;
import java.util.Iterator;
import javax.xml.stream.events.Namespace;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.events.Comment;
import javax.xml.stream.events.DTD;
import java.io.FileNotFoundException;
import javax.xml.stream.XMLStreamException;
import java.io.UnsupportedEncodingException;
import java.io.IOException;
import java.util.Stack;



class osis_milestone_paragraph_to_xml_element_1
{
    public static void main(String args[])
    {
        System.out.print("osis_milestone_paragraph_to_xml_element_1  Copyright (C) 2014-2022  Stephan Kreutzer\n" +
                         "This program comes with ABSOLUTELY NO WARRANTY.\n" +
                         "This is free software, and you are welcome to redistribute it\n" +
                         "under certain conditions. See the GNU Affero General Public\n" +
                         "License 3 or any later version for details. Also, see the source code\n" +
                         "repository https://gitlab.com/free-scriptures/free-scriptures/\n" +
                         "and the project website http://www.free-scriptures.org.\n\n");

        if (args.length < 2)
        {
            System.out.print("Usage:\n" +
                             "\tosis_milestone_paragraph_to_xml_element_1 input-file output-file\n\n");
            System.exit(1);
        }


        File inputFile = new File(args[0]);
        File outputFile = new File(args[1]);

        try
        {
            BufferedWriter writer = new BufferedWriter(
                                    new OutputStreamWriter(
                                    new FileOutputStream(outputFile),
                                    "UTF-8"));

            String delayedVerse = null;
            String delayedVerseTag = null;
            boolean inParagraph = false;

            Stack<StructureStackElement> structureStack = new Stack<StructureStackElement>();

            try
            {
                XMLInputFactory inputFactory = XMLInputFactory.newInstance();

                InputStream in = new FileInputStream(inputFile);
                XMLEventReader eventReader = inputFactory.createXMLEventReader(in);
                XMLEvent event = null;

                while (eventReader.hasNext() == true)
                {
                    event = eventReader.nextEvent();

                    if (event.isStartDocument() == true)
                    {
                        StartDocument startDocument = (StartDocument)event;

                        writer.write("<?xml version=\"" + startDocument.getVersion() + "\"");

                        if (startDocument.encodingSet() == true)
                        {
                            writer.write(" encoding=\"" + startDocument.getCharacterEncodingScheme() + "\"");
                        }

                        if (startDocument.standaloneSet() == true)
                        {
                            writer.write(" standalone=\"");

                            if (startDocument.isStandalone() == true)
                            {
                                writer.write("yes");
                            }
                            else
                            {
                                writer.write("no");
                            }

                            writer.write("\"");
                        }

                        writer.write("?>\n");
                    }
                    else if (event.isStartElement() == true)
                    {
                        QName elementName = event.asStartElement().getName();
                        String fullElementName = elementName.getLocalPart();

                        if (elementName.getPrefix().isEmpty() != true)
                        {
                            fullElementName = elementName.getPrefix() + ":" + fullElementName;
                        }

                        String output = "<" + fullElementName;

                        // http://coding.derkeiler.com/Archive/Java/comp.lang.java.help/2008-12/msg00090.html
                        @SuppressWarnings("unchecked")
                        Iterator<Namespace> namespaces = (Iterator<Namespace>)event.asStartElement().getNamespaces();

                        while (namespaces.hasNext() == true)
                        {
                            Namespace namespace = namespaces.next();

                            if (namespace.isDefaultNamespaceDeclaration() == true &&
                                namespace.getPrefix().length() <= 0)
                            {
                                output += " xmlns=\"" + namespace.getNamespaceURI() + "\"";
                            }
                            else
                            {
                                output += " xmlns:" + namespace.getPrefix() + "=\"" + namespace.getNamespaceURI() + "\"";
                            }
                        }

                        // http://coding.derkeiler.com/Archive/Java/comp.lang.java.help/2008-12/msg00090.html
                        @SuppressWarnings("unchecked")
                        Iterator<Attribute> attributes = (Iterator<Attribute>)event.asStartElement().getAttributes();

                        while (attributes.hasNext() == true)
                        {
                            Attribute attribute = attributes.next();
                            QName attributeName = attribute.getName();
                            String fullAttributeName = attributeName.getLocalPart();

                            if (attributeName.getPrefix().length() > 0)
                            {
                                fullAttributeName = attributeName.getPrefix() + ":" + fullAttributeName;
                            }

                            String attributeValue = attribute.getValue();

                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            attributeValue = attributeValue.replaceAll("&", "&amp;");
                            attributeValue = attributeValue.replaceAll("\"", "&quot;");
                            attributeValue = attributeValue.replaceAll("'", "&apos;");
                            attributeValue = attributeValue.replaceAll("<", "&lt;");
                            attributeValue = attributeValue.replaceAll(">", "&gt;");

                            output += " " + fullAttributeName + "=\"" + attributeValue + "\"";
                        }

                        output += ">";

                        if (fullElementName.equalsIgnoreCase("verse") == true)
                        {
                            delayedVerse = output;
                            delayedVerseTag = fullElementName;
                        }
                        else if (fullElementName.equalsIgnoreCase("milestone") == true)
                        {
                            Attribute attributeType = event.asStartElement().getAttributeByName(new QName("type"));

                            if (attributeType == null)
                            {
                                System.out.println("osis_milestone_paragraph_to_xml_element_1: Element '" + fullElementName + "' is missing its 'type' attribute.");
                                System.exit(1);
                            }

                            if (attributeType.getValue().equals("x-extra-p") != true &&
                                attributeType.getValue().equals("x-p") != true)
                            {
                                System.out.println("osis_milestone_paragraph_to_xml_element_1: Element '" + fullElementName + "' with unsupported value '" + attributeType.getValue() + "' in attribute 'type'.");
                                System.exit(1);
                            }

                            if (inParagraph == true)
                            {
                                StructureStackElement structureStackElement = structureStack.pop();

                                if (structureStackElement.GetElement().equalsIgnoreCase(fullElementName) != true)
                                {
                                    if (structureStackElement.GetElement().equalsIgnoreCase("p") != true)
                                    {
                                        System.out.println("osis_milestone_paragraph_to_xml_element_1: Structure stack mismatch, element '" + structureStackElement.GetElement() + "' found while element 'p' was expected.");
                                        System.exit(1);
                                    }
                                }

                                writer.write("</p>");
                                inParagraph = false;
                            }

                            if (delayedVerse != null)
                            {
                                writer.write("<p>");
                                structureStack.push(new StructureStackElement("p"));

                                writer.write(delayedVerse);
                                structureStack.push(new StructureStackElement(delayedVerseTag));

                                delayedVerse = null;
                                delayedVerseTag = null;
                                inParagraph = true;
                            }
                            else
                            {
                                System.out.println("osis_milestone_paragraph_to_xml_element_1: Element '" + fullElementName + "' found, but not immediately after 'verse' element.");
                            }
                        }
                        else
                        {
                            if (delayedVerse != null)
                            {
                                writer.write(delayedVerse);
                                structureStack.push(new StructureStackElement(delayedVerseTag));

                                delayedVerse = null;
                                delayedVerseTag = null;
                            }

                            writer.write(output);
                            structureStack.push(new StructureStackElement(fullElementName));
                        }
                    }
                    else if (event.isEndElement() == true)
                    {
                        boolean output = true;

                        QName elementName = event.asEndElement().getName();
                        String fullElementName = elementName.getLocalPart();

                        if (elementName.getPrefix().isEmpty() != true)
                        {
                            fullElementName = elementName.getPrefix() + ":" + fullElementName;
                        }

                        if (fullElementName.equalsIgnoreCase("milestone") == true)
                        {

                        }
                        else
                        {
                            if (delayedVerse != null)
                            {
                                System.out.println("osis_milestone_paragraph_to_xml_element_1: End element encountered, while still a delayed verse was left (which is expected to have been cleared before).");
                                System.exit(1);
                            }

                            StructureStackElement structureStackElement = structureStack.pop();

                            if (structureStackElement.GetElement().equalsIgnoreCase(fullElementName) != true)
                            {
                                if (structureStackElement.GetElement().equalsIgnoreCase("p") == true)
                                {
                                    writer.write("</" + structureStackElement.GetElement() + ">");
                                    inParagraph = false;
                                }
                                else
                                {
                                    System.out.println("osis_milestone_paragraph_to_xml_element_1: Unsupported element '" + structureStackElement.GetElement() + "' encountered in the structure stack, while element 'p' was expected instead.");
                                    System.exit(1);
                                }

                                structureStackElement = structureStack.pop();

                                if (structureStackElement.GetElement().equalsIgnoreCase(fullElementName) != true)
                                {
                                    System.out.println("osis_milestone_paragraph_to_xml_element_1: Structure stack mismatch, end element '" + structureStackElement.GetElement() + "' found while element '" + fullElementName + "' was expected.");
                                    System.exit(1);
                                }
                            }

                            writer.write("</" + fullElementName + ">");
                        }
                    }
                    else if (event.isCharacters() == true)
                    {
                        if (delayedVerse != null)
                        {
                            writer.write(delayedVerse);
                            structureStack.push(new StructureStackElement(delayedVerseTag));

                            delayedVerse = null;
                            delayedVerseTag = null;
                        }

                        event.writeAsEncodedUnicode(writer); 
                    }
                    else if (event.getEventType() == XMLStreamConstants.COMMENT)
                    {
                        writer.write("<!--" + ((Comment)event).getText() + "-->");
                    }
                    else if (event.getEventType() == XMLStreamConstants.DTD)
                    {
                        DTD dtd = (DTD) event;

                        if (dtd != null)
                        {
                            writer.write(dtd.getDocumentTypeDeclaration());
                        }
                    }
                    else if (event.isEndDocument() == true)
                    {

                    }
                    else
                    {
                        System.out.println("osis_milestone_paragraph_to_xml_element_1: XML Event type '" + event.getEventType() + "' (javax.xml.stream.XMLStreamConstants) not supported.");
                        System.exit(1);
                    }
                }
            }
            catch (FileNotFoundException ex)
            {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
                System.exit(1);
            }
            catch (XMLStreamException ex)
            {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
                System.exit(1);
            }
            catch (UnsupportedEncodingException ex)
            {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
                System.exit(1);
            }
            catch (IOException ex)
            {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
                System.exit(1);
            }

            writer.flush();
            writer.close();
        }
        catch (FileNotFoundException ex)
        {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            System.exit(1);
        }
        catch (UnsupportedEncodingException ex)
        {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            System.exit(1);
        }
        catch (IOException ex)
        {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            System.exit(1);
        }
    }
}
