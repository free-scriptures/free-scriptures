/* Copyright (C) 2014-2022  Stephan Kreutzer
 *
 * This file is part of osis_milestone_paragraph_to_xml_element_1.
 *
 * osis_milestone_paragraph_to_xml_element_1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * osis_milestone_paragraph_to_xml_element_1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with osis_milestone_paragraph_to_xml_element_1. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/StructureStackElement.java
 * @author Stephan Kreutzer
 * @since 2022-05-07
 */



class StructureStackElement
{
    public StructureStackElement(String element)
    {
        this.element = element;
    }

    public String GetElement()
    {
        return this.element;
    }

    protected String element;
}

